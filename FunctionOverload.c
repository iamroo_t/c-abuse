#include <stdio.h>

void printnan(void)
{
	puts("no parameter");
}

int printi(int a)
{
	puts("int");
	return 1;
}

float printfl(float a)
{
	puts("float");
	return 1.0f;
}

void printii(int a, int b)
{
	puts("int int");
}

void printif(int a, float b)
{
	puts("int float");
}

void printfi(float a, int b)
{
	puts("float int");
}

void printff(float a, float b)
{
	puts("float float");
}

#define GET_FUNCTION_NAME(_0, _1, _2, NAME, ...) NAME
#define magic(...) GET_FUNCTION_NAME(_0, ##__VA_ARGS__, printt2param, printt1param, printnan)(__VA_ARGS__)
#define printt2param(X, Y, ...) _Generic((X), \
	int: _Generic((Y), \
		int: printii, \
		float: printif),\
	float: _Generic((Y), \
		int: printfi, \
		float: printff))(X, Y)

#define printt1param(X) _Generic((X), \
	int: printi, \
	float: printfl)(X)

int main(void)
{
	magic(1.0f);
	magic(1, 1);
	magic(1.0f, 1.0f);
	magic(1, 1.0f);
	magic(1.0f, 1);
	magic();

	if(magic(1) == 1 && magic(1.0f) == 1.0f)
	{
		return 0;
	}

	return 0;
}