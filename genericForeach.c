#include <stdio.h>
#include <assert.h>
#include <stdlib.h>

/*#define TOKENPASTE2(x, y) x ## y
#define TOKENPASTE(x, y) TOKENPASTE2(x, y)*/

#define foreach(value, set) \
\
for( \
/*, splitted list of initialised values, leave out type only * or not and cast if pointer type*/\
	int foreachCounter = 0\
;\
\
_Generic((set) \
/*Tests*/\
	,int*: foreachCounter < sizeof(set)/sizeof(int) \
	,list*: value != NULL\
/*end of Tests*/\
); \
\
_Generic((value)\
/*how to increment initialised value*/\
	,int*: foreachCounter++ \
	,node*: (void)0\
)\
/*how to get next Value*/\
, _Generic((value) \
	,int*: value = (int*)(&set[foreachCounter]) \
	,node*: value = (*(node*)value).next\
))\

typedef struct node {
   int data;
   struct node *next;
} node;

typedef struct list {
	struct node *head;
	struct node *current;
} list;

void list_insert(struct list *list, int data)
{
	struct node *link = malloc(sizeof(struct node));

	link->data = data;
	link->next = list->head;
	list->head = link;
}

struct list *list_new()
{
	struct list *ret = malloc(sizeof(struct list));

	ret->head = NULL;
	ret->current = NULL;

	return ret;
}

int main(int argc, char **argv)
{
	int testIntArray[] = {1, 3, 5, 2, 12, 52};

	{
		int *intItem = &testIntArray[0];
		foreach(intItem, testIntArray)
		{
			fprintf(stdout, "%d\n", *intItem);
		}
	}
printf("==================\n");
	struct list *testList = list_new();

	{
		struct node *listItem = testList->head;
		foreach(listItem, testList)
		{
			fprintf(stdout, "%d\n", listItem->data);
		}
	}
printf("==================\n");
	list_insert(testList, 10);list_insert(testList, 20);list_insert(testList, 120);list_insert(testList, 420);
	{
		struct node *listItem = testList->head;
		foreach(listItem, testList)
		{
			fprintf(stdout, "%d\n", listItem->data);
		}
	}

	return 0;
}